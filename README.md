# Submition for Individual Game Jam

#### Made by Ariasena Cahya Ramadhani

Download link below!
https://ddelvo.itch.io/lari-pulici


LARI PULICI! is a top-down game where the player is a robber who is trying to run from the police. Don't be seen by the police and use your ultimate ability "Phantasm" to summon your inner demon.

## Diversifier (Required Feature)
- BOTW Clone: Implement stamina feature
(Running and ultimate ability will cost you stamina) 

- Berserk: Player can not be seen by the enemy (Try not to get caught by the pulici)

- Dewa Kipas: Implement not too smart AI (Well, the pulici doesn't have a high IQ so..)


## Control

Arrow Keys and Mouse: Menu control

W, A, S, D: Move

Q: Ultimate Skill

## Assets
### Sprites

Karakter: https://elthen.itch.io/

Map: https://emily2.itch.io/modern-city?download


### Sound

https://freesound.org/people/Sirkoto51/sounds/371580/

https://freesound.org/people/furbyguy/sounds/331874/

https://kenney.nl/assets/rpg-audio


Special Thanks to..
DeaconBeacon - For the help especially in programming