extends Control


func _ready():
	$HBoxContainer/Button.grab_focus()
	MusicController.play_main_menu_music()

func _on_Button_pressed():
	get_tree().change_scene("res://Scenes/Map Level 1.tscn")


func _on_Button2_pressed():
	get_tree().change_scene("res://Scenes/MainMenu.tscn")
