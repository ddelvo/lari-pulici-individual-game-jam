extends ProgressBar


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export (NodePath) var target


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var Player = get_node(target)
	if Player != null and Player.stamina <= 100:
		Player.stamina += 5*delta
	
	if Player != null:
		value = Player.stamina
