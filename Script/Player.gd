extends KinematicBody2D

var state_machine
var run_speed = 80
var dash_speed = 40
var velocity = Vector2.ZERO
var is_running_x_positive = false
var is_running_x_negative = false
var is_running_y_positive = false
var is_running_y_negative = false
var stamina = 100
onready var active_manager = $"../active_manager"


func _ready():
	state_machine = $AnimationTree.get("parameters/playback")

func get_input():
	is_running_x_positive = false
	is_running_x_negative = false
	is_running_y_positive = false
	is_running_y_negative = false
	var current = state_machine.get_current_node()
	
	var is_active = active_manager.active
	velocity = Vector2.ZERO
	
	if is_active == 1:
		$Camera2D.current = true
		if Input.is_action_pressed("right"):
			velocity.x += 1
			$Sprite.flip_h = false
			is_running_x_positive = true
		if Input.is_action_pressed("left"):
			velocity.x -= 1
			$Sprite.flip_h = true
			is_running_x_negative = true
		if Input.is_action_pressed("up"):
			is_running_y_positive = true
			velocity.y -= 1
		if Input.is_action_pressed("down"):
			is_running_y_negative = true
			velocity.y += 1
		if (is_running_x_positive or is_running_y_positive) and Input.is_action_pressed("shift") and stamina > 0:
			stamina -= 0.5
			run_speed += 40
		if (is_running_x_negative or is_running_y_negative) and Input.is_action_pressed("shift")  and stamina > 0:
			stamina -= 0.5
			run_speed += 40
		velocity = velocity.normalized() * run_speed
		run_speed = 80
		if velocity.length() == 0:
			state_machine.travel("Idle")
		if velocity.length() != 0:
			state_machine.travel("Walking")
	else:
		stamina -= 0.25
		$Camera2D.current = false
		if stamina <= 0:
			active_manager.active = 1
		state_machine.travel("Idle")
	
func _physics_process(delta):
	get_input()
	velocity = move_and_slide(velocity)

