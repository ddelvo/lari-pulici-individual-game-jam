extends KinematicBody2D

var move =  Vector2.ZERO
var run_speed = 80
var dash_speed = 40
var is_running_x_positive = false
var is_running_x_negative = false
var is_running_y_positive = false
var is_running_y_negative = false

onready var active_manager = $"../active_manager"
onready var player = $"../Player"

func _ready():
	visible = false

func get_input():
	var is_running_x_positive = false
	var is_running_x_negative = false
	var is_running_y_positive = false
	var is_running_y_negative = false
	var move =  Vector2.ZERO
	if Input.is_action_pressed("right"):
		move.x += 1
		$AnimatedSprite.flip_h = false
		is_running_x_positive = true
	if Input.is_action_pressed("left"):
		move.x -= 1
		$AnimatedSprite.flip_h = true
		is_running_x_negative = true
	if Input.is_action_pressed("up"):
		is_running_y_positive = true
		move.y -= 1
	if Input.is_action_pressed("down"):
		is_running_y_negative = true
		move.y += 1
	move = move.normalized() * 1.4
	
	if move.length() != 0:
		$AnimatedSprite.play("Walking")
	if move.length() == 0:
		$AnimatedSprite.play("Idle")
			
	move = move_and_collide(move)


func _process(delta):
	var is_active = active_manager.active
	
	if is_active == 1:
		visible = false
		$Camera2D.current = false
		var move =  Vector2.ZERO
		move = position.direction_to(player.position)
		move = move.normalized() * 1.4
		if move.length() != 0:
			$AnimatedSprite.play("Walking")
		if move.length() == 0:
			$AnimatedSprite.play("Idle")
		move = move_and_collide(move)
	else:
		visible = true
		$Camera2D.current = true
		move =  Vector2.ZERO
		get_input()
