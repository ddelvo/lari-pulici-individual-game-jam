extends KinematicBody2D
var move =  Vector2.ZERO

onready var player = $"../Player"
onready var shadow = $"../Shadow"
onready var active_manager = $"../active_manager"

var player_in_range = false
var player_in_sight = false
var is_police_sound = false
var state_machine
var speed = 1
export var flashlight_direction = Vector2()

func _ready():
	state_machine = $AnimationTree.get("parameters/playback")
	$Sight.look_at(flashlight_direction)

func _physics_process(delta):
	move = Vector2.ZERO
	SightCheck()
	if player_in_range and player_in_sight:
		if active_manager.active == 1:
			move = position.direction_to(player.position)
		else:
			move = position.direction_to(shadow.position)
	else:
		move = Vector2.ZERO

	move = move.normalized() * 1.35
	
	if move.length() == 0:
		state_machine.travel("IdlePhone")
	if move.length() != 0:
		if !is_police_sound:
			is_police_sound = true
			$PlayerInSight.play()
		state_machine.travel("Walking")
	move = move_and_collide(move)

func _on_Area2D_body_entered(body):
	if body == player or body == shadow:
		player_in_range = true


func _on_Area2D_body_exited(body):
	if body == player or body == shadow:
		player_in_range = false
		player_in_sight = false
		is_police_sound = false
	
func SightCheck():
	if player_in_range == true:
		var space_state = get_world_2d().direct_space_state
		var sight_check
		
		if active_manager.active == 1:
			sight_check = space_state.intersect_ray(global_position, player.global_position, [self], $Sight.collision_mask)
		else:
			sight_check = space_state.intersect_ray(global_position, shadow.global_position, [self], $Sight.collision_mask)

		if sight_check:
			if sight_check.collider.name != "Player" or sight_check.collider.name != "Shadow":
				player_in_sight = false
			if sight_check.collider.name == "Player":
				player_in_sight = true
				$Sight.look_at(player.global_position)
			if sight_check.collider.name == "Shadow":
				player_in_sight = true
				$Sight.look_at(shadow.global_position)

func _on_ZoneTertangkap_body_entered(body):
	if body == player:
		get_tree().change_scene("res://Scenes/GameOver.tscn")
