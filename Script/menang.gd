extends Control

func _ready():
	$Button.grab_focus()
	MusicController.play_win_music()

func _on_Button_pressed():
	get_tree().change_scene("res://Scenes/MainMenu.tscn")
