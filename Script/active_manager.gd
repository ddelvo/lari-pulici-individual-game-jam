extends Node


export var active = 1


func _process(delta):

	if Input.is_action_just_pressed("phantasm"):
		if active != 2:
			$ActivateSkill.play()
			$DemonLaugh.play()
			active += 1
		else:
			active = 1
