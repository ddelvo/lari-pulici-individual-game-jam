extends Node

var main_menu_music = load("res://Assets/Audio/mainmenu.wav")
var ingame_music = load("res://Assets/Audio/ingame.wav")
var win_music = load("res://Assets/Audio/French_Toast.mp3")

func play_main_menu_music():
	
	if $MusicPlayer.stream != main_menu_music:
		$MusicPlayer.stream = main_menu_music
		$MusicPlayer.volume_db = -7
		$MusicPlayer.play()

func play_ingame_music():
	$MusicPlayer.stream = ingame_music
	$MusicPlayer.volume_db = -8
	$MusicPlayer.play()
	
func play_win_music():
	$MusicPlayer.stream = win_music
	$MusicPlayer.volume_db = -5
	$MusicPlayer.play()
	
