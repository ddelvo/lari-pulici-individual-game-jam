extends Control

func _ready():
	$VBoxContainer/Start.grab_focus()
	MusicController.play_main_menu_music()


func _on_Button_pressed():
	get_tree().change_scene("res://Scenes/Map Level 1.tscn")


func _on_Button3_pressed():
	get_tree().quit()


func _on_Help_pressed():
	get_tree().change_scene("res://Scenes/Help.tscn")
